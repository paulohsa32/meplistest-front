(function() {
    'use strict';

    angular
        .module('sample')
        .directive('acmeNavbar', acmeNavbar);

    /** @ngInject */
    function acmeNavbar() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/navbar/navbar.html',
            scope: {
                creationDate: '='
            },
            controller: NavbarController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function NavbarController(moment, $state) {
            var vm = this;

            // "vm.creation" is avaible by directive option "bindToController: true"
            vm.relativeDate = moment(vm.creationDate).fromNow();
            vm.isCollapsed = false;
            vm.activeLink = activeLink;

            function activeLink(n) {
                var mainState = $state.current.name.split(".")[0];
                if( n == mainState ) 
                  return "active";
                return "";
            }
        }
    }

})();
