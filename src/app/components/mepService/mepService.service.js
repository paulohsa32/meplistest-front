(function () {
    "use strict";

    angular
        .module("sample")
        .factory("mepService", mepService);

    /** @ngInject */
    function mepService() {
        var svc = {
            request: request
        };

        return svc;
 
        function request(config) {
           	config.headers["Meplis-Security-Server"] = "xpto-belgium";

            return config;
        }
    }

})();
 