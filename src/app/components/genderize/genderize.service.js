(function () {
    "use strict";

    angular
        .module("sample")
        .factory("Genderize", Genderize);

    /** @ngInject */
    function Genderize($http) {
        var svc = {
            getGender: getGender
        };

        return svc;
 
        function getGender(name) {
           	return $http.get("https://api.genderize.io/?name=" + name);
        }
    }

})();
 