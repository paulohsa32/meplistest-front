(function() {
    "use strict";

    angular
        .module("sample")
        .directive('mepShare', mepShare);

    function mepShare() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/mepShare/mepShare.template.html',
            scope: {
                shareTitle: '='
            },
            link: mepShareLink
        };
        return directive;
    }

    function mepShareLink(scope, $location, el, attrs) {

        socialLinks();

        scope.$watch('shareTitle', function(newVal) {
            if (newVal)
                socialLinks();
        }, true);

        function socialLinks() {
            var url = window.location.href;
            scope.facebook = "https://www.facebook.com/sharer/sharer.php?u=" + scope.shareTitle + "&t=" + url;
            scope.twitter = "https://twitter.com/intent/tweet?source=" + url + "&text=" + scope.shareTitle;
            scope.gplus = "https://plus.google.com/share?url=" + url;
        }
    }

})();
