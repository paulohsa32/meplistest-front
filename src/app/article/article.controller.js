(function() {
    'use strict';

    angular
        .module('sample')
        .controller('ArticleController', ArticleController)
        .controller('ArticleViewController', ArticleViewController)
        .controller('ArticleEditController', ArticleEditController);

    /** @ngInject */
    function ArticleController(Article) {
        var vm = this;

        if (!Article.articles.length) {
            Article.find().then(function() {
                vm.articles = Article.articles;
            });
        } else {
            vm.articles = Article.articles;
        }

        vm.remove = function(idx) {
            Article.remove(idx);
        }

    }

    /** @ngInject */
    function ArticleViewController($state, Article, Genderize) {
        var vm = this;

        Article.findBySlug($state.params.slug).then(function(res) {
            vm.current = res;
            getAuthorGender();
        });

        function getAuthorGender(){
            var name = vm.current.authors[0].profile.name;
            name = name.split(" ")[0];
            Genderize.getGender(name).then(function(response){
                vm.current.authors[0].profile.gender = response.data.gender;
            });
        }
    }

    /** @ngInject */
    function ArticleEditController($state, Article) {
        var vm = this;
        var currentArticle = {};
        if ($state.params.slug) {
            vm.editMode = true;
            vm.prevArticle = prevArticle;
            vm.nextArticle = nextArticle;

            Article.findBySlug($state.params.slug).then(function(res) {
                vm.current = res;
                currentArticle = res;
            });

            var articles = [];

            function prevArticle() {
                Article.findPrev($state.params.slug).then(function(res) {
                    $state.go($state.current, {slug: res.slug}, {reload: true});
                });
            }

            function nextArticle() {
                Article.findNext($state.params.slug).then(function(res) {
                    $state.go($state.current, {slug: res.slug}, {reload: true});
                });
            }
        }

        vm.save = function(article) {
            Article.save(article).then(function() {
                console.log(vm.current.slug);
                $state.go("articles.edit", {slug: vm.current.slug}, {reload: true});
            });
        };
    }
})();
